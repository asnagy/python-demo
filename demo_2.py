import random
from pprint import pprint
from typing import Counter

if __name__ == '__main__':
    print('Hello in demo 2')

    list_ = [i for i in range(20)]
    pprint(list_)
    print('---------------------')
    set_ = {i for i in range(20)}
    pprint(set_)
    print('---------------------')
    dict_ = dict([(i, i) for i in range(20)])
    pprint(dict_)
    print('---------------------')

    for i, j in zip(list_, list_[1:]):
        print(i,j)
    print('---------------------')
    for i, j in enumerate(list_[::-1]):
        print(i,j)
    print('---------------------')
    for i, (j, k) in enumerate(zip(list_, list_[1:])):
        print(i, j, k)
    print('---------------------')

    for i, j in enumerate(zip(list_, list_[1:])):
        print(i, j)
    print('---------------------')

    r = [random.randint(0,100) for i in range(1000)]
    sr = list(sorted(dict(Counter(r)).items(), key= lambda x: x[1], reverse=True))
    pprint(sr[:5])
